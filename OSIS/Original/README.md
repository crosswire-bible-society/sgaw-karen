# Notes on individual XML file[s]

## Psalms.xml
1. Added title tags for the canonical Psalm titles.
2. Some of these (e.g. Ps.5) were paragraphed, but not all of them (e.g. Ps.6).
3. There were 117 Psalms thus tagged. It's well-known that 34 Psalms have no title, which leaves 116 that should.
4. Psalm 111 had such a title but shouldn't. 2011-12-12 Edited Psalms.xml and notified Levi Thang.
5. Psalm 119 had U+1000 MYANMAR LETTER KA just before verse 1, so I removed it.
6. Also added a paragraph tag before Psalm 119.48 and after each stanza.